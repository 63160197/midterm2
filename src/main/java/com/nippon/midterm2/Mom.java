/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nippon.midterm2;

/**
 *
 * @author Nippon
 */
public class Mom extends Family{
    
    public Mom(String name, String surname, int age, int height) {
        super(name, surname, 37, 160);
        System.out.println("Mom Created");
        System.out.println("Mom : " + name+ " Surname : " + surname + " Age : " + age + " Height : " + height);
        System.out.println("----------");
    }
    @Override
    public void sleep(){
        super.sleep();
        System.out.println("Mom : "+name+" mom sleep 10 pm.");
    }
    @Override
    public void eat(){
        super.eat();
        System.out.println("Mom : " + name + " mom eat noodle");
    }
    public static void name(String name){
        System.out.println("Hello , " +name.toString());
    }
    
    
}
