/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nippon.midterm2;

import static com.nippon.midterm2.Family.name;

/**
 *
 * @author Nippon
 */
public class TestFamily {

    public static void main(String[] args) {
        Family family = new Family("Name", "Surname", 0, 0);
        family.sleep();
        family.eat();
        System.out.println("----------");

        Mom foon = new Mom("Foon", "Woratayanun", 37, 160);
        foon.sleep();
        foon.eat();
        System.out.println("----------");
        name("Mom is Family : " + (foon instanceof Family));
        name("Mom have a child : " + (foon instanceof Mom));
        System.out.println("Have a good day :-)");
        System.out.println("----------");
        System.out.println("----------");
        surname();

        Sister naple = new Sister("Naple", "Supapipat", 10, 148);
        naple.sleep();
        naple.eat();
        System.out.println("----------");
        name("Sister is Family : " + (naple instanceof Family));
        name("Sister have a mom : " + (naple instanceof Sister));
        System.out.println("Have a good day :-)");
        System.out.println("----------");
        System.out.println("----------");
        surname();

        Me nippon = new Me("Nippon", "Worataynun", 20, 164);
        nippon.sleep();
        nippon.eat();
        System.out.println("----------");
        name("Me is Family : " + (nippon instanceof Family));
        name("Me have a mom : " + (nippon instanceof Me));
        System.out.println("Have a good day :-)");
        System.out.println("----------");
        System.out.println("----------");
        surname();
    }

    private static void surname() {
        System.out.println("-------------Bye------------");
    }

}
