/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nippon.midterm2;

/**
 *
 * @author Nippon
 */
public class Sister extends Family{
    
    public Sister(String name, String surname, int age, int height) {
        super(name, surname, 10, 148); 
        System.out.println("Sister Created");
        System.out.println("Sister : " + name+ " Surname : " + surname + " Age : " + age + " Height : " + height);
        System.out.println("----------");
    }
    @Override
    public void sleep(){
        super.sleep();
        System.out.println("Sister : " + name +  " sister sleep 3 am.");
    }
    @Override
    public void eat(){
        super.eat();
        System.out.println("Sister : " + name + " sister eat snack");
    }
}
